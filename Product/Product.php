<?php
include_once './StoreProduct.php';

abstract class Product
{
	private $scraper;

	private $name;
	private $price;
	protected $model;
	private $coupon;
	private $rewrds;
	private $manufacturerPart;

	public function __construct($name, $price, $coupon, $rewards, $manufacturerPart) {
		$this->name = $name;
		$this->price = $price;
		$this->coupon = $coupon;
		$this->rewards = $rewards;
		$this->manufacturerPart = $manufacturerPart;
	}

	abstract public function setModel();

	public function getName() {
		return $this->name;
	}

	public function getPrice() {
		$this->price;
	}

	public function getCoupon() {
		return $this->coupon;
	}

	public function getRewards() {
		return $this->rewards;
	}

	public function getManufacturerPart() {
		return $this->manufacturerPart;
	}

	public function getModel() {
		return $this->model;
	}

	public function getStoreProduct() {
		return new StoreProduct($this->name, $this->model, $this->price);
	}
}

?>