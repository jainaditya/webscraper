<?php
include_once './Product.php';
include_once '../ProductExtractor.php';

class Monitor extends Product
{
	public function setModel() {
		if ($this->getName()) {
			$this->model = $this->parseModel();
		}
	}

	protected function parseModel() {
		preg_match('/.+ \| (.*)/is', $this->getName(), $match);
		if ($match[1]) return $match[1];
	}
}

$productExtractor = new ProductExtractor('https://www.dell.com/en-us/shop/accessories/apd/210-agnk?c=us&amp%3Bl=en&amp%3Bs=dhs&amp%3Bcs=19&amp%3Bsku=210-AGNK');
$extract = $productExtractor->extract();

$product = new Monitor(
	$extract['name'],
	$extract['price'],
	$extract['coupon'],
	$extract['rewards'],
	$extract['manufacturerPart']
);
$product->setModel();
print_r($product->getStoreProduct());
?>