# WebScraper

PHP library to scrape a web page by a given URL. The content of the page gets indexed by tag name. The content of text can be accesses by using a css like selector or tag name and attribute to filter the results.
eg: `h1.product-name` or `div#price`.

## Usage
```
# For Products on Dell.com
Run `AllProducts.php` in `/scrapper/Product/` to get desired output described in the exercise.
```

## Usage on other sites
```
$scraper = new Scraper('https://www.amazon.com/dp/B07FK9H65N/ref=fs_a_mb_3');
echo 'Name: '. $scraper->find('h1#title')->text();
echo 'Price: '. $scraper->find('span#priceblock_ourprice')->text();
```

### Future Features

This library can be improved upon by allowing `find` function to accept nested selectors.

There will new methods in `HTMLNode` class to keep track of parent, children, siblings of the node.

Implement caching to cache the contents of the page.
