<?php
class HTMLNode
{
	protected $node;
	protected $nodeName;
	protected $attr;
	
		
	protected function __construct($node, $nodeName = null, $attr = null) {
		$this->node = $node;
		$this->nodeName = $nodeName;
		$this->attr = $attr;
	}
	
	public function html() {
		return $this->node;
	}
	
	public function text() {
		return trim(html_entity_decode(strip_tags($this->html()), ENT_QUOTES));
	}
	
	public function attr() {
		return $this->attr;
	}
	
	public function nodeName() {
		return $this->nodeName;
	}
	
	// TODO: Add parsing for nested selectors
	public static function parseSelector($sel) {
		$sel = preg_replace('/\\s*(>|,)\\s*/', '$1', $sel);
		return preg_split('/[.|#]/', $sel);
	}
}
?>