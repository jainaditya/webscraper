<?php
include_once 'HTMLNode.php';

class HTMLElement extends HTMLNode
{
	public function __get($name) {
		switch($name) {
			case 'html'     : return $this->html();
			case 'text'     : return $this->text();
			case 'attr'     : return $this->attr();
			case 'nodeName' : return $this->nodeName();
		}
	}		
}
?>