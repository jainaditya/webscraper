<?php
include_once 'HTMLNode.php';
include_once 'HTMlElement.php';

class Scraper extends HTMLNode
{
	public static $skip_tags = array('script', 'link', 'html', 'head', 'meta', 'title', 'noscript', 'br', 'style', 'body');
	public $tags; 
	protected static $arr = array();

	public function __construct($url, $indexing = true) {
		$this->html = file_get_contents("$url");
		$this->tags = self::$arr;
		if ($this->html && $indexing)
			$this->indexAllTags($this->html);
	}
	
	protected function indexAllTags($html, $tags = array()) {
		preg_match_all('/<([a-z0-9\-]+)(.*?)>((.*?)<\/\1>)?/is', $html, $matches);
		if (count($matches[0]) != 0){
			for ($i = 0; $i < count($matches[0]); $i++){
				if (!in_array(trim($matches[1][$i]), self::$skip_tags)) {
					$node = trim($matches[4][$i]);
					$nodeName = trim($matches[1][$i]);
					$attr = trim($matches[2][$i]);
					$this->tags[] = new HTMLElement($node, $nodeName, $attr);
				}
				
				// find all children
				if (trim($matches[4][$i]) != '' and preg_match('/<[a-z0-9\-]+.*?>/is', $matches[4][$i])){
					$this->indexAllTags($matches[4][$i]);
				}	
			}
		}
	}
	
	public  function find($sel, $attr = null) {
		$parsed = self::parseSelector($sel);
		$tagName = $parsed[0];
		$tagMatch = $parsed[1];
		
		if ($attr !== null) $tagMatch = $attr;		
		
		if ($tagName != '') {
			preg_match_all('/<'.$tagName.' .*?>(.*?)<\/'.$tagName.'>/is', $this->html, $matches);
			foreach ($matches[0] as $key => $value) {
				preg_match('/<([a-z0-9\-]+) .*'.$tagMatch.'.*?>((.*?)<\/\1>)/is', $value, $match);
				if (count($match)) {
					return new HTMLElement($value, false);
				}
			}
		} else {
				$this->error = 'Could not select tag: "'.$tagname.'('.$num.')"!';
				return false;
			}
	}
	
	public function findByClassName($class = null)
	{
		$found = array();
		if ($this->tags) {
			foreach ($this->tags as $key => $el) {
				preg_match('/.*'.$class.'.*/i', $el->attr(), $matches);
				if (count($matches)) {
					$found[] = $this->tags[$key];
				}
			}
		}
		return $found;
	}
}
?>