<?php
include_once 'Scraper.php';
include_once 'Product/StoreProduct.php';

class ProductExtractor
{
	public static $nameSelector = 'h1#sharedPdPageProductTitle';
	public static $priceSelector = 'span';
	public static $priceAttr = 'sharedPSPDellPrice';
	public static $couponSelector = 'h5';
	public static $couponAttr = 'top-padding-7 bottom-offset-5 text-blue force-bold-font';
	public static $rewrdsSelector = 'a.rewards-popover';
	public static $manufacturerPartSelector = 'text-capitalize text-gray-sepia-light small-font';

	private $scraper;

	public function __construct($url) {
		$this->scraper = new Scraper($url);
	}

	public function getStoreProduct() {
		$product = $this->extract();
		return new StoreProduct($this->name, $this->model, $this->price);
	}

	public function extractName() {
		$name = $this->scraper->find(self::$nameSelector);
		if ($name) return $name->text();
	}

	public function extractPrice() {
		$price = $this->scraper->find(self::$priceSelector, self::$priceAttr);
		if ($price) {
			$floatPrice = floatval(preg_replace('/[^0-9.]/', '', $price->text()));
			return number_format($floatPrice, 2);;
		}
	}

	public function extractCouponCode() {
		$coupon = $this->scraper->find(self::$couponSelector, self::$couponAttr);
		if ($coupon) {
			preg_match('/[a-z0-9\-]+ coupon code: (.*)/is', $coupon->text(), $match);
			return match[1];
		}
	}

	public function extractRewards() {
		$rewards = $this->scraper->find(self::$rewrdsSelector);
		if ($rewards) return (int)preg_replace('/[^0-9]/', '', $rewards->text());
	}

	public function extractManufacturerPart() {
		$manufacturerPart = $this->scraper->findByClassName(self::$manufacturerPartSelector);
		if (count($manufacturerPart) > 1) {
			foreach ($manufacturerPart as $el) {
				preg_match('/Manufacturer part (.+)/', $el->text(), $match);
				if ($match[1]) return trim($match[1]);
			}
		}
	}

	public function extract() {
		$name = $this->extractName();
		$price = $this->extractPrice();
		$coupon = $this->extractCouponCode();
		$rewards = $this->extractRewards();
		$manufacturerPart = $this->extractManufacturerPart();

		return array(
			'name' => $name,
			'price' => $price,
			'coupon' => $coupon,
			'rewards'=> $rewards,
			'manufacturerPart' => $manufacturerPart
		);
	}
}
?>